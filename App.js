import React from 'react';
import { AppLoading, Asset, Font, Icon, SplashScreen, LinearGradient } from 'expo';
import { StyleSheet, View} from 'react-native';

import Loader from 'react-native-mask-loader';
import AppNavigator from './navigation/AppNavigator';

export default class App extends React.Component {
  state = {
    animateReady: false,
    appReady: false,
    rootKey: Math.random()
  };

  constructor() {
    super();
    this._image = require('./assets/images/bitloft-icon.png');
  }

  componentDidMount() {
    this.setState({animateReady: true});

    setTimeout(() => {
      this.setState({appReady: true});
    }, 350);
  }

  render() {


    if (!this.state.appReady) {
      return (
        <View key={this.state.rootKey} style={styles.root}>
  
          <Loader
            isLoaded={this.state.animateReady}
            imageSource={this._image}
            backgroundStyle={styles.loadingBackgroundStyle}
          />
  
        </View>
      );
    }

    return (
      <AppNavigator/>
    );

  }

}

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loadingBackgroundStyle: {
    backgroundColor: '#2c323b',
  },
});