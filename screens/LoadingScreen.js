import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';

export default class LoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this._navigate();
  }

  _navigate = async () => {
    // this is typically async in case we need to gather some device information.

    this.props.navigation.navigate('App');
  };

  // Render any loading content here
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
});
