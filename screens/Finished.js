import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    Animated,
    TouchableOpacity,
    Text,
} from 'react-native';

import { LinearGradient } from 'expo';
import { SkypeIndicator } from 'react-native-indicators';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height; 

export default class Finished extends Component {

    constructor(props) {
        super(props);

        var visiting = 
        
        this.opacity = new Animated.Value(0);
        this.getStartedOpacity = new Animated.Value(0);

        this.state = {
            isLoading: false,
            firstName: props.navigation.state.params.firstName,
            visiting: props.navigation.state.params.visiting,
        }
    }

    componentWillMount = async () => {
        
        this.fadeOpacity(this.opacity);

        setTimeout(() => {
            this.fadeOpacity(this.getStartedOpacity);
        }, 750);
    }


    fadeOpacity = (prop) => {
        Animated.timing(prop, {
            toValue: 1,
            duration: 750,
        }).start();
    }

    render() {

        var loader;
        if (this.state.isLoading) {
            loader = (
                <View style={styles.loaderWrapper}>
                    <SkypeIndicator color='white' />
                </View>
            );
        } else {
            loader = null;
        }

        return (
            <LinearGradient 
                colors={['white', '#fafafa']} 
                style={styles.linearGradient}>

                {loader}

                <View style={styles.welcomeWrapper}>

                    <Animated.Image style={[styles.logo, {opacity: this.opacity}]}
                        source={require('../assets/images/bitloft-logo.png')}
                        resizeMode='contain' />

                    <Animated.Text style={[styles.welcomeText, {opacity: this.opacity}]} >
                        Thanks, {this.state.firstName}
                    </Animated.Text>

                    <Animated.Text style={[styles.visitingText, {opacity: this.opacity}]} >
                        {this.state.visiting} will be with you shortly
                    </Animated.Text>

                    <Animated.View style={[styles.buttonWrapper, {opacity: this.getStartedOpacity}]} >
                    
                        <TouchableOpacity style={styles.backButton} onPress={() => this.props.navigation.navigate('Welcome')}>
                            <Text style={styles.buttonText} >Back</Text>
                        </TouchableOpacity>
                    
                    </Animated.View>

                </View>                       

            </LinearGradient>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    linearGradient: {
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center'
    },
    loaderWrapper: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        zIndex: 9999999,
        backgroundColor: 'rgba(0,0,0,.3)',
        height: height
    }, 
    welcomeWrapper: {
        width: width,
        height: height,
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    logo: {
        width: width * .7,
    },
    welcomeText: {
        fontSize: 42,
        color: '#2c323b',
        marginBottom: 32,
        fontWeight: 'bold',     
    }, 
    visitingText: {
        fontSize: 22,
        color: '#2c323b',
        marginBottom: 32,
        fontWeight: 'bold',     
    }, 
    buttonWrapper: {
        flexDirection: 'row',
        width: width * .7,
        justifyContent: 'center',
        marginBottom: 32,
    },
    backButton: {
        backgroundColor: '#e62125',
        padding: 16,
        borderRadius: 15,
        margin: 16,
    },
    buttonText: {
        color: 'white',
        fontSize: 22,
    }
});
