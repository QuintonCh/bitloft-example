import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    Keyboard,
    TouchableWithoutFeedback,
    KeyboardAvoidingView,
    AsyncStorage,
    Animated,
    TextInput,
    TouchableOpacity,
    Text,
    ScrollView,
} from 'react-native';

import { LinearGradient } from 'expo';
import { SkypeIndicator } from 'react-native-indicators';
import firebase from 'firebase';

// We'll import something for the datepicker.
import DatePicker from 'react-native-datepicker';

// Let's use a modal picker for picking who we will be visiting...
import ModalFilterPicker from 'react-native-modal-filter-picker';

import { TextInputMask } from 'react-native-masked-text';

// Cheeky, but super helpful.
import _ from 'lodash';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height; 

const DismissKeyboardHOC = (Comp) => {
    return ({ children, ...props }) => (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <Comp {...props}>
          {children}
        </Comp>
      </TouchableWithoutFeedback>
    );
  };

const DismissKeyboardView = DismissKeyboardHOC(View);

let available_persons = [];

export default class NewGuest extends Component {

    constructor(props) {
        super(props);
        
        this.opacity = new Animated.Value(0);
        this.formFade = new Animated.Value(0);
        this.signUserIn = this.signUserIn.bind(this);

        this.state = {
            isLoading: false,
            datepickerVisible: false,
            emailAddress: '',
            firstName: '',
            visiting: 'Who are you visiting?',
            visitingModalVisible: false,
            reasonForVisit: '',
        }
    }

    async saveItem(item, selectedValue) {
        try {
            await AsyncStorage.setItem(item, selectedValue);
        } catch (error) {
            console.error('AsyncStorage error: ' + error.message);
        }
    }

    componentWillMount = async () => {

        // We need to get our lists of people who can be visited.
        firebase.database().ref('available_persons/').once('value', function(snapshot) {

            available_persons = Object.values(snapshot.val()).map((data, i) => {

                return ({
                    key: i,
                    label: data.name
                });

            });
        });

        this.fadeOpacity(this.opacity);

        setTimeout(() => {
            this.fadeOpacity(this.formFade);
        }, 750);
    }


    fadeOpacity = (prop) => {
        Animated.timing(prop, {
            toValue: 1,
            duration: 750,
        }).start();
    }

    signUserIn = () => {

        Keyboard.dismiss();

        if (this.state.emailAddress === '')
            alert('Please enter an email address');

        if (this.state.visiting === 'Who are you visiting?')
            alert('Please let us know who you are visiting');


        if (this.state.visiting !== 'Who are you visiting?' && this.state.emailAddress !== '') {

            this.setState({isLoading: true});

            var db = firebase.database();
            var ref = db.ref('visitors').orderByChild('EmailAddress').equalTo(this.state.emailAddress);
            ref.once('value', (snapshot) => {

                console.log(snapshot);

                if (snapshot.exists()) {

                    Object.values(snapshot.val()).map((data) => {

                        this.props.navigation.navigate('Finished', {
                            firstName: data.FirstName, visiting: this.state.visiting,
                        });

                    });

                    // We should handle a case where there are more than one record typically, 
                    // but that can always be done down the road.

                } else {

                    // This is a new user, redirect them to the new user screen.

                    this.props.navigation.navigate('NewGuest');
                }

            });

            
        }

    }

    updateVisiting = (visiting) => {
        this.setState({visiting});
    }

    render() {

        var loader;
        if (this.state.isLoading) {
            loader = (
                <View style={styles.loaderWrapper}>
                    <SkypeIndicator color='white' />
                </View>
            );
        } else {
            loader = null;
        }

        return (

            <KeyboardAvoidingView style={styles.container} behavior="padding">
                
                <DismissKeyboardView>

                    <LinearGradient 
                    colors={['white', '#fafafa']} 
                    style={styles.linearGradient} />

                    {loader}

                    <ScrollView
                        bounces={true}
                        pinchGestureEnabled={false}
                        showsVerticalScrollIndicator={false}
                        style={styles.scrollViewContainer} >

                        <View style={styles.welcomeWrapper}>

                            <Animated.Image style={[styles.logo, {opacity: this.opacity}]}
                                source={require('../assets/images/bitloft-logo.png')}
                                resizeMode='contain' />

                            <Animated.Text style={[styles.createText, {opacity: this.opacity}]} >
                                Please enter your email address and your reason for visiting. We will handle the rest.
                            </Animated.Text>

                            <Animated.View style={[styles.introWrapper, {opacity: this.formFade}]}>

                                <View style={styles.formWrapper}>

                                    <View style={styles.inputWrapper}>
                                    
                                        <TextInput 
                                            placeholder='Your Email Address' 
                                            placeholderTextColor='rgba(44,50,59,.35)'
                                            autoCapitalize='none' 
                                            style={styles.textInput}
                                            keyboardType='email-address'
                                            value={this.state.emailAddress}
                                            onChangeText={(emailAddress) => this.setState({emailAddress})} />

                                    </View>

                                    <View style={styles.inputWrapper}>
                                        
                                        <TouchableOpacity style={styles.textInput} onPress={() => this.setState({visitingModalVisible : true})}>

                                            <Text style={ this.state.visiting === 'Who are you visiting?' ? styles.placeholderText : styles.selectedText}>{this.state.visiting}</Text>

                                        </TouchableOpacity>
                                    
                                    </View>

                                    <ModalFilterPicker
                                        visible={this.state.visitingModalVisible}
                                        onSelect={(value) => this.setState({ visiting: _.filter(available_persons, { key: value })[0].label, visitingModalVisible: false })}
                                        onCancel={() => this.setState({visitingModalVisible: false})}
                                        options={available_persons}
                                        listContainerStyle={styles.modalPickerContainer}
                                        cancelButtonStyle={styles.cancelButtonStyle}
                                        cancelButtonTextStyle={styles.cancelButtonTextStyle} />

                                    <View style={styles.inputWrapper}>
                                    
                                        <TextInput 
                                            placeholder='Visit Reason' 
                                            placeholderTextColor='rgba(44,50,59,.35)'
                                            autoCapitalize='none' 
                                            style={styles.textInput}
                                            value={this.state.reasonForVisit}
                                            onChangeText={(reasonForVisit) => this.setState({reasonForVisit})} />
                                    
                                    </View>

                                    <View style={styles.submitButtonWrapper}>
                                        
                                        <TouchableOpacity 
                                            onPress={() => this.signUserIn()}
                                            style={styles.nextButton}> 
                                            
                                            <Text style={styles.submitButtonText}>
                                                Sign In
                                            </Text>

                                        </TouchableOpacity>

                                    </View>
                                
                                </View>

                            </Animated.View>

                        </View>                       

                    
                    </ScrollView>
                    
                   

                </DismissKeyboardView>

            </KeyboardAvoidingView>
            
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    linearGradient: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        zIndex: 1150,
    },
    scrollViewContainer: {
        position: 'relative',
        flex: 1,
        zIndex: 1250,
        width: width,
    },
    loaderWrapper: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        zIndex: 9999999,
        backgroundColor: 'rgba(0,0,0,.3)',
        height: height
    }, 
    welcomeWrapper: {
        width: width,
        height: height,
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    logo: {
        width: width * .7,
        height: 200,
    },
    createText: {
        marginBottom: 45,
    },

    introWrapper: {
        alignItems: 'center',
    },
    formWrapper: {
        alignItems: 'center',
        alignContent: 'center',
    },
    inputWrapper: {
        marginBottom: 15,
        width: width * .5,
        backgroundColor: 'transparent',
        paddingTop: 12,
        paddingBottom: 12,
        borderColor: 'transparent',
        borderBottomWidth: 2,
        borderBottomColor: '#2c323b', 
    },
    textInput: {
        color: '#2c323b',
        fontWeight: 'bold',
        fontSize: 18,
    },
    dateInput: {
        marginBottom: 15,
        width: width * .5,
        backgroundColor: 'transparent',
        paddingTop: 12,
        paddingBottom: 12,
    },
    modalPickerContainer: {
        borderRadius: 15,
        width: width * .9,
        height: height * .6,
        backgroundColor: 'white',
        marginBottom: 15,
        paddingBottom: 10,
    },
    optionTextStyle: {
        fontSize: 18,
        color: '#33343f',
    },
    cancelButtonStyle: {
        flex: 0,
        width: width * .5,
        backgroundColor: '#82bebd',
        borderRadius: 15,
        paddingTop: 10,
        paddingBottom: 10,
    },
    cancelButtonTextStyle: {
        fontSize: 20,
        color: 'white',
        textAlign: 'center',
    },
    placeholderText: {
        color: 'rgba(44,50,59,.35)',
        fontWeight: 'bold',
        fontSize: 18,
    }, 
    selectedText: {
        color: '#2c323b',
        fontWeight: 'bold',
        fontSize: 18,
    },
    submitButtonWrapper: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    nextButton: {
        backgroundColor: '#e62125',
        padding: 16,
        borderRadius: 15,
        margin: 16,
    },
    submitButtonText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18,
    },
    buttonWrapper: {
        flexDirection: 'row',
        marginTop: 32,
    }
});
