import React, { Component } from 'react';
import {
    View, 
    Button,
    TouchableOpacity,
    Image,
    Text,
    StyleSheet,
} from 'react-native';

export class GoogleButton extends React.Component {

    render() {

        return (

            <TouchableOpacity style={styles.googleButton} onPress={() => this.props.action()} >
            
                <Image style={styles.logo} resizeMode='contain' source={require('../assets/images/google_logo.png')} />

                <Text style={styles.text}>Sign in with Google</Text>

            </TouchableOpacity>

        );

    }

}

const styles = StyleSheet.create({
    googleButton: {
        width: 225,
        height: 48,
        backgroundColor: 'white',
        shadowColor: '#2c323b',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: .125,
        shadowRadius: 10,
        borderColor: 'rgba(44,50,59,.1)',
        borderWidth: 1,
        alignSelf: 'stretch',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8,
        margin: 8,
    },
    logo: {
        height: 28,
        width: 28,
        marginRight: 8,
    },
    text: {
        fontSize: 16,
        color: '#2c323b',
    }
});