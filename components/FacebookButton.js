import React, { Component } from 'react';
import {
    View, 
    Button,
    TouchableOpacity,
    Image,
    Text,
    StyleSheet,
} from 'react-native';

export class FacebookButton extends React.Component {

    render() {

        return (

            <TouchableOpacity style={styles.facebookButton} onPress={() => this.props.action()} >
            
                <Image style={styles.logo} resizeMode='contain' source={require('../assets/images/facebook_logo_white.png')} />

                <Text style={styles.text}>Sign in with Facebook</Text>

            </TouchableOpacity>

        );

    }

}

const styles = StyleSheet.create({
    facebookButton: {
        width: 225,
        height: 48,
        backgroundColor: '#3c5a9a',
        shadowColor: '#2c323b',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: .125,
        shadowRadius: 10,
        borderColor: 'rgba(44,50,59,.1)',
        borderWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8,
        margin: 8,
    },
    logo: {
        height: 28,
        width: 28,
        marginRight: 8,
    },
    text: {
        fontSize: 16,
        color: 'white',
    }
});