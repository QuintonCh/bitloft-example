import React from 'react';
import { createAppContainer, createStackNavigator, createSwitchNavigator } from 'react-navigation';

// import MainTabNavigator from './MainTabNavigator';
import LoadingScreen from '../screens/LoadingScreen';
import Welcome from '../screens/Welcome';
import NewGuest from '../screens/NewGuest';
import ReturningGuest from '../screens/ReturningGuest';
import Finished from '../screens/Finished';

const AppStack = createStackNavigator(
  { Welcome: Welcome, NewGuest: NewGuest, ReturningGuest: ReturningGuest, Finished: Finished },
  { headerMode : 'none' }
);


export default createAppContainer(createSwitchNavigator(
  // You could add another route here for authentication.
  // Read more at https://reactnavigation.org/docs/en/auth-flow.html
  // Main: MainTabNavigator,
  {
    AuthLoading: LoadingScreen,
    App: AppStack,
    // Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading'
  }
));